# Documentation for openkvs

Install:

    $ pip3 install sphinx
    $ pip3 install sphinxcontrib-httpdomain
    $ make html
    $ make pdf


Available for browsing at http://barrel-db.gitlab.io/openkvs-doc/
