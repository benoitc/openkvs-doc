Erlang API Reference
====================

Module openkvs
--------------

   This module is the main entry point to manipulate an openkvs store.
   It handles the connection to the store and all the documents
   creation, modification and retrieval operations.

.. erl:module:: openkvs

.. erl:function:: start_db(Name, Opts)

   :param Name: Storage name
   :type Name: atom()
   :param Name: Opts
   :type Name: options()
   :rtype: ``{ok, pid(), uri()}``

   Starts a supervised process to manage an openkvs storage.

   `Names` is an atom used to retrieve the local process.

   Available options are:

   * ``{backend, Backend}``: define the type of backend to acces. Allowed
     backends are:

     * ``ets``: in memory ETS table
     * ``rocksdb``: rocksdb storage.

   If the database is local, it will start it.

   If the database is remote, it will create a proxy allowing creation of new
   connections.

   It returns the `Uri` to be used for creating new connection with
   `openkvs:connect`.

.. erl:function:: stop_db(Uri)

   Stop the the process associated with the storage at `Uri`.

.. erl:function:: connect(Uri)

   :rtype: ``{ok, connection()}``

   Create a new connection to the store found by ``Uri``. Could be a local
   store, in the same Erlang node, or a remote node. In the later case, it will
   create TCP connection to the storage using the native storage protocol.

.. erl:function:: get(Conn, Key, Opts)

   :rtype: ``{ok, key(), revision()}``

   Retrieve a document from the storage, for the given key and revision.

   Returns ``{error, not_found}`` if the document can not be found.

.. erl:function:: put(Conn, Key, Value, Opts)

   :rtype: ``{ok, key(), revision()} | {error, conflict}``

   Create or update a document in the storage.

   If the option ``{db_version, Revision}`` is provided, it will ensure
   the last known version in the storage has the same revision. If not,
   it will returns ``{error, conflict}``.

.. erl:function:: delete(Conn, Key, Opts)

   :rtype: ``{ok, key()}``

   Delete a document.

.. erl:function:: get_next(Conn, Key)

   :rtype: ``{ok, key(), value(), revision()} | {error, not_found}``

   Return information about the document with the next superior key.

.. erl:function:: get_previous(Conn, Key)

   :rtype: ``{ok, key(), value(), revision()} | {error, not_found}``

   Return information about the document with the previous inferior key.

.. erl:function:: get_keyrange(Conn, StartKey, EndKey, Max, Opts)

   :rtype: ``[{ok, key(), value(), revision()}]``

   Return the list of key/value (with associated revision) stored between the
   keys ``StartKey`` and ``EndKey``. This can be used to browse the storage for
   available documents.

   The following options are available:

   ``{startkey_inclusive, boolean()}``: includes (or not) startkey in results.
   ``{endkey_inclusive, boolean()}``: includes (or not) endkey in results.

