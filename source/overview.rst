Overview
========

OpenKVS is key/value storage for Erlang applications that you can query locally
and remotely in a transparent manner.

**Features**

* Concurrent readers and writers
* CAS semantic
* Atomic write operations (batch)
* Transparent connection to remote or local storage using a simple URI.
* Disk & memory storage using rocksdb or ETS
* Optimised to handle high read and write load over the network
* A connection can be shared by multiple processes.
* Multiple connections can co-exist in the same vm

**Sample usage:**

.. code-block:: erlang

   1> application:ensure_all_started(openkvs).
   {ok,[syntax_tools,compiler,goldrush,lager,ranch,erocksdb,
        worker_pool,openkvs]}
   2> {ok, _, Uri} = openkvs:start_db(test, []).
   {ok,<0.226.0>,"openkvs:ets://localhost:4985"}
   3> Uri.
   "openkvs:ets://localhost:4985"
   4>  {ok, Conn} = openkvs:connect(Uri).
   {ok,{openkvs_local,test}}
   5> openkvs:get(Conn, <<"a">>).
   {error,not_found}
   6> openkvs:put(Conn, <<"a">>, 1, []).
   {ok,<<"a">>,1475451782}
   7> openkvs:put(Conn, <<"a">>, 1, [{db_version, 1}]).
   {error,conflict}
   8> openkvs:put(Conn, <<"a">>, 1, [{db_version, 1475451782}]).
   {ok,<<"a">>,1475451826}

