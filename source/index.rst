.. barrel-db documentation master file, created by
   sphinx-quickstart on Sun Sep 18 18:19:07 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to openkvs's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 2

   overview.rst
   erlang_api.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

